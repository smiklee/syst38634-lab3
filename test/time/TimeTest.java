/*
 * Author: Elizabeth Smikle
 * ID: 991515967
 * Program: Computer Programmer
 * Project: <Insert Project Name Here>
 */
package time;

import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author elizabethsmikle
 */
@RunWith( Parameterized.class )
public class TimeTest {
    public String timeToTest;
    public int seconds;
    
    public TimeTest( String dataTime, int seconds ) {
        timeToTest = dataTime;
        this.seconds = seconds;
    }
    
    @Parameterized.Parameters
    public static Collection<Object [ ]> loadData(){
        Object[][] data = { { "12:05:05", 43505}, { "12:05:05",43505}};
        return Arrays.asList(data);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTotalSeconds method, of class Time.
     */
    @Test
    public void testGetMilliSeconds(){
        int millis = Time.getMilliSeconds("12:05:05:888");
        System.out.println(millis);
        assertTrue("The milliseconds were not calculated properly", millis == 888);
    }
    
    @Test (expected = NumberFormatException.class )
    public void testGetMilliSecondsException(){
        int millis = Time.getMilliSeconds("12:05:05:5555");
    }
    
    @Test
    public void testGetMilliSecondsBoundaryIn(){
        int millis = Time.getMilliSeconds("12:05:05:999");
        System.out.println(millis);
        assertTrue("The milliseconds were not calculated properly", millis == 999);
    }
    
    @Test (expected = NumberFormatException.class )
    public void testGetMilliSecondsBoundaryOut(){
        int millis = Time.getMilliSeconds("12:05:05:1000");
    }
    
//    @Test
//    public void testGetTotalSeconds() {
//        System.out.println("getTotalSeconds");
//        String time = "";
//        int expResult = 0;
//        int result = Time.getTotalSeconds(time);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of getSeconds method, of class Time.
     */
//    @Test
//    public void testGetSeconds() {
//        System.out.println("getSeconds");
//        String time = "";
//        int expResult = 0;
//        int result = Time.getSeconds(time);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of getTotalMinutes method, of class Time.
     */
//    @Test
//    public void testGetTotalMinutes() {
//        System.out.println("getTotalMinutes");
//        String time = "";
//        int expResult = 0;
//        int result = Time.getTotalMinutes(time);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of getTotalHours method, of class Time.
     */
//    @Test
//    public void testGetTotalHours() {
//        System.out.println("getTotalHours");
//        String time = "";
//        int expResult = 0;
//        int result = Time.getTotalHours(time);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
