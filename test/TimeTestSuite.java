/*
 * Author: Elizabeth Smikle
 * ID: 991515967
 * Program: Computer Programmer
 * Project: <Insert Project Name Here>
 */

import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Suite;
import time.Time;

/**
 *
 * @author elizabethsmikle
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({})
public class TimeTestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @RunWith(Parameterized.class)
    public class TimeTest {

        public String timeToTest;

        public TimeTest(String dataTime) {

            timeToTest = dataTime;

        }

        @Parameterized.Parameters
        public Collection<Object[]> loadData() {

            Object[][] data = {{"12:05:05"}};
            return Arrays.asList(data);

        }

        @Test
        public void testGetTotalSeconds() {
            int seconds = Time.getTotalSeconds(this.timeToTest);
            System.out.println(this.timeToTest);
            assertTrue("The seconds were not calculated properly", seconds == 43505);
        }
        
        @Test
        public void testBoundaryGetTotalSeconds(){
            
        }
    }
}
